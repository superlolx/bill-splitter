package com.example.gabriel.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    EditText preco;
    EditText nPessoas;
    EditText precoInd;
    EditText nome;
    ArrayList<ProdutoIndividual> lista = new ArrayList<ProdutoIndividual>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                adicionarIndividual(v);
            }
        });

        final Button button4 = findViewById(R.id.button3);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finalizar(v);
            }
        });
    }

    public void adicionarIndividual(View view) {
        precoInd = (EditText) findViewById(R.id.editText3);
        nome = (EditText) findViewById(R.id.editText4);
        double precoIndDado = Double.parseDouble(preco.getText().toString());
        String nomeDado = nPessoas.getText().toString();
        ProdutoIndividual prod = new ProdutoIndividual(precoIndDado,nomeDado);
        lista.add(prod);


    }

    public void finalizar(View view) {
        preco = (EditText) findViewById(R.id.editText2);
        nPessoas = (EditText) findViewById(R.id.editText);
        double precoDado = Double.parseDouble(preco.getText().toString());
        int nPessoasDado = Integer.parseInt(((nPessoas.getText().toString())));
        Bundle bundle = new Bundle();

        Intent intent = new Intent(this, MainActivity.class);

        intent.putExtra("EXTRA_PRECO", precoDado);
        intent.putExtra("EXTRA_PESSOAS", nPessoasDado);
        bundle.putSerializable("EXTRA_LISTA", lista);
        intent.putExtras(bundle);
        startActivity(intent);


    }
}
