package com.example.gabriel.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Main3Activity extends AppCompatActivity {

    TextView text;


    double preco;
    int nPessoas;
    ArrayList<ProdutoIndividual> lista = new ArrayList<ProdutoIndividual>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Intent intent = getIntent();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                preco= 0;
                nPessoas= 0;
                lista= null;
            } else {
                preco= extras.getDouble("EXTRA_PRECO");
                nPessoas= extras.getInt("EXTRA_PESSOAS");
                Bundle bundle = intent.getExtras();
                lista = (ArrayList<ProdutoIndividual>) bundle.getSerializable("EXTRA_LISTA");
                text = (TextView) findViewById(R.id.textView);
                text.setText(preco+"\n"+nPessoas+"\n"+lista.get(0).getNome());

            }
        } else {
            preco= (double) savedInstanceState.getSerializable("EXTRA_PRECO");
            nPessoas= (int) savedInstanceState.getSerializable("EXTRA_PESSOAS");
            lista = (ArrayList<ProdutoIndividual>) savedInstanceState.getSerializable("EXTRA_LISTA");
        }
    }
}
